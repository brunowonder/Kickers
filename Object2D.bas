﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.9
@EndOfDesignText@
Private Sub Class_Globals
    Public id         As String
    Public status     As String
    Public shape      As String
    Public p_buffer   As Vector2D
    Public v_buffer   As Vector2D
    Public position   As Vector2D
    Public velocity   As Vector2D
    Public collision  As Vector2D
    Public size       As Vector2D
    Public radius     As Double
    Public friction   As Double
    Public mass       As Double
End Sub

Public Sub Initialize(x As Double, y As Double)
    position.Initialize(x, y)
    velocity.Initialize(0, 0)
    collision.Initialize(0, 0)
End Sub

Public Sub SetSize(width As Double, height As Double)
    size.Initialize(width, height)
    radius = Max(size.x, size.y) / 2
End Sub

Public Sub SetRadius(r As Double)
    radius = r
    size.Initialize(r * 2, r * 2)
End Sub

Public Sub SetMass(m As Double)
    mass = m
End Sub

Sub getX As Double
    Return position.x
End Sub

Sub getY As Double
    Return position.y
End Sub

Sub Update_Velocity(angle As Double, speed As Double)
    If speed < 0 Then Log(speed)
    velocity.Update_AngleMagnitude(angle, Min(radius * 1.99, speed))
    'velocity.Update_AngleMagnitude(angle, speed)
End Sub

Sub Update_Position(x As Double, y As Double)
    position.Update_Components(x, y)
End Sub

Sub Update_Status(new_status As String)
    status = new_status
End Sub

Sub getSpeed As Double
    Return velocity.magnitude
End Sub

Sub getAngle As Double
    Return velocity.angle
End Sub

Sub getWidth As Double
    Return size.x
End Sub

Sub getHeight As Double
    Return size.y
End Sub

Sub Left As Double
    Return getX - getWidth / 2
End Sub

Sub Top As Double
    Return getY - getHeight / 2
End Sub

Sub Apply_Velocity(friction_demultiplier As Double)
    Update_Position(                           _
        position.x + Cos(getAngle) * getSpeed, _
        position.y + Sin(getAngle) * getSpeed  )
    Update_Velocity(getAngle, getSpeed * friction_demultiplier)
    Backup
End Sub

Sub Backup
    p_buffer.Initialize(position.x, position.y)
    v_buffer.Initialize(velocity.x, velocity.y)
End Sub