﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.9
@EndOfDesignText@
Private Sub Class_Globals
    Public id               As String
    Public maxSpeed         As Double
    Public actionRadius     As Double
    Public status           As String
    Public body             As Object2D
    Public to_ball          As Vector2D
    Public to_goal          As Vector2D
End Sub

Public Sub Initialize(x As Double, y As Double)
    body.friction = 0
    body.Initialize(x, y)
    to_ball.Initialize(0, 0)
    to_goal.Initialize(0, 0)
End Sub

Sub getDistanceToBall As Double
    Return to_ball.magnitude
End Sub

Sub getAngleToBall As Double
    Return to_ball.angle
End Sub

Sub getDistanceToGoal As Double
    Return to_goal.magnitude
End Sub

Sub getAngleToGoal As Double
    Return to_goal.angle
End Sub

Sub getX As Double
    Return body.position.x
End Sub

Sub getY As Double
    Return body.position.y
End Sub

Sub getPosition As Vector2D
    Return body.position
End Sub

Sub getSpeed As Double
    Return body.velocity.magnitude
End Sub

Sub getAngle As Double
    Return body.velocity.angle
End Sub

Sub getRadius As Double
    Return body.radius
End Sub

Sub Update_Velocity(angle As Double, speed As Double)
    body.Update_Velocity(angle, speed)
    'body.velocity.Update_AngleMagnitude(angle, Min(getRadius * 1.99, speed))
End Sub

Sub Update_Position(x As Double, y As Double)
    body.position.Update_Components(x, y)
End Sub

Sub Update_Status(new_status As String)
    status = new_status
End Sub

Sub Apply_Velocity(friction_demultiplier As Double)
    body.Apply_Velocity(friction_demultiplier)
End Sub

Sub Turn_To_Goal(radians As Double)
    Dim new_angle As Double
    If to_goal.angle > body.velocity.angle Then
        new_angle = Min(to_goal.angle, body.velocity.angle + radians)
    Else
        new_angle = Max(to_goal.angle, body.velocity.angle - radians)
    End If
    Update_Velocity(new_angle, body.velocity.magnitude)
End Sub