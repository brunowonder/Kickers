﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.9
@EndOfDesignText@
#IgnoreWarnings: 30

Private Sub Process_Globals
    Type MouseInteraction(oX As Double, oY As Double, x As Double, y As Double, isPressed As Boolean, onPlayer As Boolean)
    Public  mouse   As MouseInteraction
    Public  player  As Player
    Private team    As Team
    Private opnt    As Team
    Private objects As List
End Sub

Sub Initialize(t As Team, o As Team)
    team = t
    opnt = o
    mouse.Initialize
    objects.Initialize
End Sub

Sub ButtonPress(x As Double, y As Double)
    If Main.isGoal Then Main.isGoal = False
    '--------------------------------------
    mouse.isPressed = True
    mouse.onPlayer  = False
    mouse.oX = x
    mouse.oY = y
    mouse.x  = x
    mouse.y  = y
    objects.Clear
    objects.AddAll(team.Players)
    objects.AddAll(opnt.Players)
    For Each player As Player In objects
        Dim point As Object2D : point.Initialize(x, y)
        If Math.isInRadius(point, player.body) Then
            mouse.onPlayer = True
            mouse.oX = player.x
            mouse.oY = player.y
            mouse.x  = mouse.oX
            mouse.y  = mouse.oY
            Exit
        End If
    Next
End Sub

Sub ButtonDrag(x As Double, y As Double)
    mouse.x  = x
    mouse.y  = y
    mouse.isPressed = True
End Sub

Sub ButtonRelease
    If mouse.onPlayer Then
        Dim origin As Vector2D
        Dim target As Vector2D
        origin.Initialize(mouse.oX, mouse.oY)
        target.Initialize(mouse.x, mouse.y)
        player.Update_Velocity(                                   _
            Math.Angle_Between(origin, target),                   _
            Math.Distance(origin, target) * player.maxSpeed * 0.05)
        SendMouse
        mouse.onPlayer = False
    End If
    mouse.isPressed = False
End Sub

Sub SendMouse
    If Network.isHost == False Then
        Dim objects As List
        objects.Initialize
        Dim m As MouseInteraction
        m.Initialize
        m.isPressed = mouse.isPressed
        m.onPlayer  = mouse.onPlayer
        m.oX        = mouse.oX
        m.oY        = mouse.oY
        m.x         = mouse.x
        m.y         = mouse.y
        objects.Add(m)
        Network.Send(objects)
    End If
End Sub

Sub HandleRemoteMouse(remote_mouse As MouseInteraction)
    Dim objects As List
    objects.Initialize
    objects.AddAll(team.Players)
    objects.AddAll(opnt.Players)
    For Each p As Player In objects
        Dim point As Object2D : point.Initialize(remote_mouse.oX, remote_mouse.oY)
        If Math.isInRadius(point, p.body) Then Exit
    Next
    Dim origin As Vector2D
    Dim target As Vector2D
    origin.Initialize(p.x, p.y)
    target.Initialize(remote_mouse.x, remote_mouse.y)
    p.Update_Velocity(                                   _
        Math.Angle_Between(origin, target),              _
        Math.Distance(origin, target) * p.maxSpeed * 0.05)
End Sub