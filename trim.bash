#!/bin/bash
sed -i 's/\t/    /g' *.b4j
sed -i 's/\s*$//g'   *.b4j
sed -i 's/\t/    /g' *.bas
sed -i 's/\s*$//g'   *.bas
for i in `ls *.b4j *.bas`; do
	if [ -f "$i" ]; then
		unix2dos $i
	fi
done
