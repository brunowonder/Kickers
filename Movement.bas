﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.9
@EndOfDesignText@
#IgnoreWarnings: 30, 9

Private Sub Process_Globals
    Private team       As Team
    Private opnt       As Team
    Private ball       As Object2D
    Private goal       As Object2D
    Private goal2      As Object2D
    Private field      As Object2D
    Private objects    As List
End Sub

Public Sub Initialize(f As Object2D, t As Team, b As Object2D, g As Object2D, o As Team)
    field = f
    team  = t
    ball  = b
    goal  = g
    opnt  = o
    objects.Initialize
    objects.add(ball)
    For Each player As Player In team.Players
        objects.add(player.body)
    Next
    For Each player As Player In opnt.Players
        objects.add(player.body)
    Next

    goal2.Initialize(field.Width - goal.X, goal.Y)
    goal2.SetSize(goal.Width, goal.Height)
End Sub

Sub Apply_Physics
    If Not(Network.isHost) Then Return
    For Each host As Object2D In objects
        Solve_Wall_Collision(host)
        For Each guest As Object2D In objects
            If host == guest Or Math.isInRadius(host, guest) == False Then Continue
            Correct_Position(host, guest)
            Correct_Position(guest, host)
            host.Backup : guest.Backup
            Solve_Collision(host, guest)
            Solve_Collision(guest, host)
        Next
        host.Apply_Velocity(host.friction * field.friction)
    Next

    If Math.isPointInRect(ball.position, goal) Or Math.isPointInRect(ball.position, goal2) Then
        Main.isGoal = True
        Set_Ball_Position(field.size.x * 0.5, field.size.y * 0.5)
    End If
End Sub

Sub Correct_Position(host As Object2D, guest As Object2D)
    host.collision.Update_Angle(                         _
        Math.Angle_Between(host.p_buffer, guest.p_buffer))
    host.Update_Position(                                                         _
        guest.x - Cos(host.collision.angle) * (host.radius + guest.radius + 0.1), _
        guest.y - Sin(host.collision.angle) * (host.radius + guest.radius + 0.1)  )
End Sub

Sub Solve_Collision(host As Object2D, guest As Object2D)
    host.Update_Velocity(                 _
        host.collision.angle + cPI,       _
        Abs(Math.PostCollisionVelocity(   _
            host.v_buffer.magnitude,      _
            guest.v_buffer.magnitude,     _
            host.mass, guest.mass, 0.75)) )
End Sub

Sub Solve_Wall_Collision(host As Object2D)
    If host.x + host.radius > field.width Then
        host.position.x = field.width - host.radius
        Bounce(host, False)
    Else if host.x - host.radius < 0 Then
        host.position.x = host.radius
        Bounce(host, False)
    End If
    If host.y + host.radius > field.height Then
        host.position.y = field.height - host.radius
        Bounce(host, True)
    Else if host.y - host.radius < 0 Then
        host.position.y = host.radius
        Bounce(host, True)
    End If
End Sub

Sub Bounce(host As Object2D, vertical As Boolean)
    Dim vX, vY, newVelocity, newAngle As Double
    newVelocity = Abs(Math.PostCollisionVelocity(host.speed, 0, 10, 1000, 0.75))
    vX = Cos(host.angle) * newVelocity
    vY = Sin(host.angle) * newVelocity
    If vertical Then
        newAngle = ATan2(-vY, vX)
    Else
        newAngle = ATan2(vY, -vX)
    End If
    host.Update_Velocity(newAngle, newVelocity)
    host.Backup
End Sub

Sub Set_Ball_Position(x As Double, y As Double)
    ball.status = ""
    ball.Update_Position(x, y)
    ball.Update_Velocity(ball.Angle, 0)
    ball.Backup
End Sub