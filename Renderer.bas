﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.9
@EndOfDesignText@
#IgnoreWarnings: 30

Private Sub Class_Globals
    Private fx     As JFX
    Private fnt    As Font
    Private cvs    As Canvas
    Private team   As Team
    Private opnt   As Team
    Private ball   As Object2D
End Sub

Public Sub Initialize(form As Form, t As Team, b As Object2D, o As Team)
    fnt = fx.CreateFont("monospace", 16, True, False)
    cvs.Initialize("cvs")
    form.RootPane.AddNode(cvs, 0, 0, form.Width, form.Height)
    team = t
    ball = b
    opnt = o
End Sub

Sub Clear
    cvs.DrawRect(0, 0, cvs.Width, cvs.Height, fx.Colors.Black, True, 0)
End Sub

Sub Draw_Everything
    Clear
    Draw_Field
    Draw_Team
    Draw_Opponents
    Draw_Ball
    DrawClickVector
    Draw_Goal
End Sub

Sub Draw_Field
    cvs.DrawImage(AssetManager.Get("field"), 0, 0, cvs.Width, cvs.Height)
End Sub

Sub Draw_Opponents
    For Each player As Player In opnt.Players
        'Draw_Player_Action_Radius(player)
        'Draw_Player_Arrow(player)
        Draw_Player(player, fx.Colors.Blue)
    Next
End Sub

Sub Draw_Team
    For Each player As Player In team.Players
        'Draw_Player_Action_Radius(player)
        'Draw_Player_Arrow(player)
        Draw_Player(player, fx.Colors.Red)
    Next
End Sub

Sub Draw_Player(player As Player, color As Paint)
    cvs.DrawCircle(player.x, player.y, player.radius - 1,   color,           True,  0)
    cvs.DrawCircle(player.x, player.y, player.radius,       fx.Colors.Black, False, 3)
    cvs.DrawText  (player.id, player.x, player.y + 5, fnt,  fx.Colors.White, "CENTER")
End Sub

Sub Draw_Player_Action_Radius(player As Player)
    cvs.DrawCircle(                               _
        player.x, player.y, player.actionRadius,  _
        fx.Colors.ARGB(192, 0, 255, 255), True, 0 )
End Sub

Sub Draw_Player_Arrow(player As Player)
    Dim a = player.angle As Double
    Dim cosA = Cos(a), sinA = Sin(a) As Double
    Dim arrow_x  = player.x + cosA * player.actionRadius      As Double
    Dim arrow_y  = player.y + sinA * player.actionRadius      As Double
    Dim arrow_ax = arrow_x  + Cos(a - Math.T_QUARTER_PI) * 10 As Double
    Dim arrow_ay = arrow_y  + Sin(a - Math.T_QUARTER_PI) * 10 As Double
    Dim arrow_bx = arrow_x  + Cos(a + Math.T_QUARTER_PI) * 10 As Double
    Dim arrow_by = arrow_y  + Sin(a + Math.T_QUARTER_PI) * 10 As Double

    cvs.DrawLine  (player.x, player.y, arrow_x, arrow_y, fx.Colors.Yellow, 2)
    cvs.DrawLine  (arrow_x, arrow_y, arrow_ax, arrow_ay, fx.Colors.Yellow, 2)
    cvs.DrawLine  (arrow_x, arrow_y, arrow_bx, arrow_by, fx.Colors.Yellow, 2)
End Sub

Sub Draw_Ball
    Dim s1 = 0.3846 * ball.radius As Double
    Dim s2 = 0.6153 * ball.radius As Double
    Dim s3 = 0.2307 * ball.radius As Double
    cvs.DrawCircle(ball.x     , ball.y     , ball.radius - 1, fx.Colors.White,  True, 0)
    cvs.DrawCircle(ball.x     , ball.y     , ball.radius    , fx.Colors.Black, False, 3)
    cvs.DrawCircle(ball.x     , ball.y     ,          s1    , fx.Colors.Black,  True, 0)
    cvs.DrawCircle(ball.x - s2, ball.y - s2,          s3    , fx.Colors.Black,  True, 0)
    cvs.DrawCircle(ball.x - s2, ball.y + s2,          s3    , fx.Colors.Black,  True, 0)
    cvs.DrawCircle(ball.x + s2, ball.y - s2,          s3    , fx.Colors.Black,  True, 0)
    cvs.DrawCircle(ball.x + s2, ball.y + s2,          s3    , fx.Colors.Black,  True, 0)
End Sub

Sub Draw_Goal
    If Not(Main.isGoal) Or SinD(DateTime.Now * 3) < 0 Then Return
    Dim f = fx.CreateFont("monospace", 122, True, False) As Font
    cvs.DrawText("GOAL!!!", cvs.Width / 2 + 5, cvs.Height / 2 + 35, f, fx.Colors.Black, "CENTER")
    cvs.DrawText("GOAL!!!", cvs.Width / 2 + 0, cvs.Height / 2 + 30, f, fx.Colors.White, "CENTER")
End Sub

Sub DrawClickVector
    Dim oX = UserInteraction.mouse.oX As Double
    Dim oY = UserInteraction.mouse.oY As Double
    Dim color As Paint
    Dim a = ATan2(                                          _
        UserInteraction.mouse.y - UserInteraction.mouse.oY, _
        UserInteraction.mouse.x - UserInteraction.mouse.oX  ) As Double
    color = fx.Colors.Red
    If UserInteraction.mouse.isPressed == False Then Return
    '-------------------------------------------------------------------------------
    If UserInteraction.mouse.onPlayer Then
        color = fx.Colors.Cyan
        oX = UserInteraction.player.x + Cos(a) * (UserInteraction.player.Radius + 2)
        oY = UserInteraction.player.y + Sin(a) * (UserInteraction.player.Radius + 2)
        cvs.DrawCircle(UserInteraction.player.x, UserInteraction.player.y, UserInteraction.player.Radius + 2, color, False, 2)
        Dim pA, pB  As Vector2D
        pA.Initialize(UserInteraction.player.x, UserInteraction.player.y)
        pB.Initialize(UserInteraction.mouse.x, UserInteraction.mouse.y)
        If Math.Distance(pA, pB) <= UserInteraction.player.Radius Then Return
    End If
    '-------------------------------------------------------------------------------
    Dim arrow_x  = UserInteraction.mouse.x As Double
    Dim arrow_y  = UserInteraction.mouse.y As Double
    Dim arrow_ax = arrow_x  + Cos(a - Math.T_QUARTER_PI) * 10 As Double
    Dim arrow_ay = arrow_y  + Sin(a - Math.T_QUARTER_PI) * 10 As Double
    Dim arrow_bx = arrow_x  + Cos(a + Math.T_QUARTER_PI) * 10 As Double
    Dim arrow_by = arrow_y  + Sin(a + Math.T_QUARTER_PI) * 10 As Double
    cvs.DrawLine(oX, oY, UserInteraction.mouse.x, UserInteraction.mouse.y, color, 2)
    cvs.DrawLine(arrow_x, arrow_y, arrow_ax, arrow_ay, color, 2)
    cvs.DrawLine(arrow_x, arrow_y, arrow_bx, arrow_by, color, 2)
End Sub

Sub cvs_MouseDragged(mouse As MouseEvent)
    UserInteraction.ButtonDrag(mouse.x, mouse.y)
End Sub

Sub cvs_MousePressed(mouse As MouseEvent)
    UserInteraction.ButtonPress(mouse.x, mouse.y)
End Sub

Sub cvs_MouseReleased(mouse As MouseEvent)
    UserInteraction.ButtonRelease
End Sub