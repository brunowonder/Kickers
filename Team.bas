﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.9
@EndOfDesignText@
Private Sub Class_Globals
    Public Players(11) As Player
End Sub

Public Sub Initialize(field As Object2D, jsonFile As String)
    Dim jParser As JSONParser
    jParser.Initialize(File.ReadString(File.DirAssets, jsonFile))
    Dim player_list = jParser.NextArray As List
    Dim i = 0 As Int
    Dim x, y As Double
    Dim position As List
    For Each player As Player In Players
        jParser.Initialize(player_list.Get(i))
        position = jParser.NextArray
        x = position.Get(0)
        y = position.Get(1)
        player.Initialize(field.Width * x, field.Height * y)
        player.body.mass = 10 'Rnd(70, 100)
        player.body.radius = 13
        player.body.friction = 0.96
        player.actionRadius = 3.5 * player.Radius
        player.maxSpeed = Rnd(100, 200) / 100
        player.body.id = i
        player.id = i
        i = i + 1
    Next
End Sub