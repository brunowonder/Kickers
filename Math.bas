﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.9
@EndOfDesignText@
#IgnoreWarnings: 12

Private Sub Process_Globals
    Public Const TWO_PI       = 2 * cPI       As Double
    Public Const HALF_PI      = cPI / 2       As Double
    Public Const QUARTER_PI   = cPI / 4       As Double
    Public Const T_QUARTER_PI = 3 * (cPI / 4) As Double
End Sub

Sub PostCollisionVelocity(u1 As Double, u2 As Double, m1 As Double, m2 As Double, CoR As Double) As Double
    Dim v1 = ((u1 * (m1 - m2)) + (2 * m2 * u2)) / (m1 + m2) As Double
    Return v1 * CoR
End Sub

Sub Distance(a As Vector2D, b As Vector2D) As Double
    Dim dX = b.x - a.x As Double
    Dim dY = b.y - a.y As Double
    Return Sqrt(dX * dX + dY * dY)
End Sub

Sub Vector_Sum(a As Vector2D, b As Vector2D) As Vector2D
    Dim result As Vector2D
    result.Initialize(a.x + b.x, a.y + b.y)
    Return result
End Sub

Sub Angle_Between(a As Vector2D, b As Vector2D) As Double
    Return ATan2(b.y - a.y, b.x - a.x)
End Sub

Sub PyMod(x As Double, y As Double) As Double
    Return (y + (x Mod y)) Mod y
End Sub

Sub Clamp(value As Double, minimum As Double, maximum As Double) As Double
    Return Max(minimum, Min(maximum, value))
End Sub


Sub isInRadius(a As Object2D, b As Object2D) As Boolean
    Return Distance(a.position, b.position) <= (a.radius + b.radius)
End Sub

Sub isPointInRect(point As Vector2D, rect As Object2D) As Boolean
    Return point.x >= rect.Left And point.x <= rect.Left + rect.Width _
       And point.y >= rect.Top  And point.y <= rect.Top  + rect.Height
End Sub
