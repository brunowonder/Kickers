﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.9
@EndOfDesignText@
Private Sub Class_Globals
    Public x, y      As Double
    Public angle     As Double
    Public magnitude As Double
End Sub

Sub Initialize(x_component As Double, y_component As Double) As Boolean
    x = x_component
    y = y_component
    Calculate_Magnitude
    Calculate_Angle
    Return True
End Sub

Sub Calculate_Angle As Double
    angle = ATan2(y, x)
    Return angle
End Sub

Sub Calculate_Magnitude As Double
    magnitude = Sqrt((x * x) + (y * y))
    Return magnitude
End Sub

Sub Calculate_X_Component As Double
    x = Cos(angle) * magnitude
    Return x
End Sub

Sub Calculate_Y_Component As Double
    y = Sin(angle) * magnitude
    Return y
End Sub

Sub Update_Angle(new_angle As Double) As Boolean
    angle = new_angle
    Calculate_X_Component
    Calculate_Y_Component
    Calculate_Magnitude
    Return True
End Sub

Sub Update_Magnitude(new_magnitude As Double) As Boolean
    magnitude = new_magnitude
    Calculate_X_Component
    Calculate_Y_Component
    Calculate_Angle
    Return True
End Sub

Sub Update_X_Component(new_x As Double) As Boolean
    x = new_x
    Calculate_Magnitude
    Calculate_Angle
    Return True
End Sub

Sub Update_Y_Component(new_y As Double) As Boolean
    y = new_y
    Calculate_Magnitude
    Calculate_Angle
    Return True
End Sub

Sub Update_Components(new_x As Double, new_y As Double) As Boolean
    x = new_x
    y = new_y
    Calculate_Magnitude
    Calculate_Angle
    Return True
End Sub

Sub Update_AngleMagnitude(new_angle As Double, new_magnitude As Double) As Boolean
    angle = new_angle
    magnitude = new_magnitude
    Calculate_X_Component
    Calculate_Y_Component
    Return True
End Sub