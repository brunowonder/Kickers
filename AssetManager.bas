﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.9
@EndOfDesignText@
Private Sub Process_Globals
    Private textures As Map
End Sub

Sub Assets As List
   Private joFiles As JavaObject = Me
   Return joFiles.RunMethod("JarContent", Array(File.DirApp & "/main.jar"))
End Sub

Sub Load_Textures
    textures.Initialize
    For Each asset As String In Assets
        If Not(asset.EndsWith(".jpg") Or asset.EndsWith(".png")) Then Continue
        asset = Regex.Split("/", asset)(1)
        Dim tex As Image : tex.Initialize(File.DirAssets, asset)
        Dim key = Regex.Split("\.", asset)(0) As String
        textures.Put(key, tex)
        Log("Texture loaded: " & asset)
    Next
End Sub

Sub Get(texture As String) As Image
    Return textures.Get(texture)
End Sub

#If JAVA
import java.io.*;
import java.util.*;
import java.util.zip.*;

//List the content of a jar archive
//Example of getting the File Assets from a Jar - which are stored in the Files folder:
//    Private joFiles As JavaObject = Me
//    Dim f As List = joFiles.RunMethod("JarContent", Array("<Here FullPathToTheJarFile>"))
//    If f = Null Then Return
//    Log("Total number of files found in the Jar: " & f.size)
//    Log("Asset Files:")
//    For i = 0 To f.size - 1
//        Dim s As String = f.Get(i)
//        If s.StartsWith("Files") Then Log(f.Get(i))
//    Next
//
public static List<String> JarContent(String searchpath) {
    List<String> files = new ArrayList<String>();
    try {
        ZipFile zipFile = new ZipFile(searchpath);
        Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
        while (zipEntries.hasMoreElements()) {
            files.add(zipEntries.nextElement().getName());
        }
        return files;
    } catch (IOException ioe) {
        ioe.printStackTrace();
        return null;
    }
}
#End If