﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=6.01
@EndOfDesignText@
'Static code module
Sub Process_Globals
    Private socket1 As Socket
    Private server As ServerSocket
    Private astream As AsyncStreams

    Public  isHost As Boolean
    Public  isInitialized As Boolean

    Public     objects As List
End Sub

Sub Initialize(args() As String)
    isHost = True
    isInitialized = False
    If args.Length < 4 Then Return

    Dim mode        = args(0) As String
    Dim local_port  = args(1) As Int
    Dim remote_port = args(2) As Int
    Dim ip_address  = args(3) As String

    Select mode
        Case "guest"
            isHost = False
            Log("Initializing GUEST mode...")

        Case "host"
            Log("Initializing HOST mode...")

        Case Else
            Return

    End Select
    server.Initialize(local_port, "server")
    server.Listen
    socket1.Initialize("socket1")
    socket1.Connect(ip_address, remote_port, 5000)
    isInitialized = True
End Sub

Sub Socket1_Connected (Successful As Boolean)
    If Successful Then
        If astream.IsInitialized Then astream.Close
        astream.InitializePrefix(socket1.InputStream, False, socket1.OutputStream, "astream")
        Log("Status: Connected")
    'Else
    '    Log(LastException)
    End If
End Sub

Sub Server_NewConnection (Successful As Boolean, NewSocket As Socket)
    If Successful Then
        If astream.IsInitialized Then astream.Close
        astream.InitializePrefix(NewSocket.InputStream, False, NewSocket.OutputStream, "astream")
        Log("Status: Listening...")
    'Else
    '    Log(LastException)
    End If
    server.Listen
End Sub

Sub Send(obj As Object)
    If astream.IsInitialized Then
        Dim serializer As B4XSerializator
        astream.Write(serializer.ConvertObjectToBytes(obj))
    End If
    'mouse.onPlayer = False
End Sub

Sub astream_NewData (Buffer() As Byte)
    Dim inStream As InputStream
    inStream.InitializeFromBytesArray(Buffer, 0, Buffer.Length)
    '----------------------------------------------------------
    Dim serializer As B4XSerializator
    objects = serializer.ConvertBytesToObject(Buffer)
End Sub

Sub astream_Error
    'Log("Error: " & LastException)
    astream.Close
    astream_Terminated
End Sub

Sub astream_Terminated
    Log("Status: Disconnected")
End Sub